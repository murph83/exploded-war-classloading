package org.jboss.as.quickstarts.kitchensink_ear.model;

import java.util.function.Consumer;

public class SharedDynamicLoader {
    private static SharedDynamicLoader ourInstance = new SharedDynamicLoader();

    public static SharedDynamicLoader getInstance() {
        return ourInstance;
    }

    private SharedDynamicLoader() {
    }

    public void loadAndInvokeCallback(String callbackClass) {
        try {
            ClassLoader cl = Thread.currentThread().getContextClassLoader();
            Consumer<String> callback = Class.forName(callbackClass,true, cl).asSubclass(Consumer.class).newInstance();
            callback.accept("Test");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }
}
