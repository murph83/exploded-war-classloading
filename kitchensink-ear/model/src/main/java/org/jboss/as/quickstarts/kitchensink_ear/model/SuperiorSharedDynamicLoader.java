package org.jboss.as.quickstarts.kitchensink_ear.model;

import java.util.function.Consumer;

public class SuperiorSharedDynamicLoader {

    private static SuperiorSharedDynamicLoader ourInstance = new SuperiorSharedDynamicLoader();

    public static SuperiorSharedDynamicLoader getInstance() {
        return ourInstance;
    }

    private SuperiorSharedDynamicLoader() {
    }

    public void loadAndInvokeCallback(String callbackClass, ClassLoader cl) {
        try {
            Consumer<String> callback = Class.forName(callbackClass,true, cl).asSubclass(Consumer.class).newInstance();
            callback.accept("Superior Test");
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }
}
