package rh.demo;

import java.util.function.Consumer;

public class Callback implements Consumer<String> {

    @Override
    public void accept(String s) {
        System.out.println("Callback invoked with: " + s);
    }
}
